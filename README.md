# Docker PostgreSQL with Governor

## Usage
The `docker-compose.yml` in this repository provides an example of running a postgres cluster.

```bash
docker-compose up
docker-compose scale postgres=3
```

This creates three postgres containers (with one as the master and others as standby), an etcd container, and an HAProxy container.

## Environment variables
 Variable             | Default                  | Description
----------------------|--------------------------|----------------------------
PGDATA                | /var/lib/postgresql/data |
ETCD_ENDPOINT         | empty                    | This is the HTTP endpoint of etcd. e.g.: `http://etcd:2379`
REPLICATION_NETWORK   | 0.0.0.0/0                | The network that the postgres cluster belongs to
REPLICATION_USER      | replicator               | The user used for replication
REPLICATION_PASS      | rep-pass                 | The password for the replication user
GOV_LOG_LEVEL         | INFO                     | Controls the log level for the Governor logger
DOMAIN                | empty                    | Append a domain to the listen address which defaults to just `${HOSTNAME}`. e.g.: `DOMAIN=.test.internal` and `HOSTNAME=hello`, `listen=hello.test.internal`
BDR_ENABLED           | false                    | Enables Bi-directional Replication (multi-master)
DATABASES             | postgres                 | A comma-separated list of databases to create (if they do not exist) and enable BDR on if `BDR_ENABLED` is true


## Governor
The container uses a forked version of an orchestration script called [Governor](https://github.com/compose/governor). The config for Governor gets created at runtime in the `docker-entrypoint` from the environment variables passed to the container. This can be found in `governor-setup.sh`.