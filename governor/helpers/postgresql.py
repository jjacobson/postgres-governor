import os, psycopg2, re, time
import logging

from urlparse import urlparse


logger = logging.getLogger(__name__)

class Postgresql:

    def __init__(self, config):
        self.name = config["name"]
        self.host, self.port = config["listen"].split(":")
        self.data_dir = config["data_dir"]
        self.replication = config["replication"]
        self.databases = config['databases'].split(',') if config['databases'] is not None else []
        self.bdr = config['bdr']

        self.config = config

        self.cursor_holder = None
        self.connection_string = "postgres://%s:%s@%s:%s/postgres" % (self.replication["username"], self.replication["password"], self.host, self.port)

        self.conn = None

    def cursor(self, dbname='postgres'):
        if not self.cursor_holder:
            self.conn = psycopg2.connect(self.local_connection_string(dbname=dbname))
            self.conn.autocommit = True
            self.cursor_holder = self.conn.cursor()

        return self.cursor_holder

    def local_connection_string(self, dbname='postgres'):
        connection_string = ["dbname=%s" % dbname, "port=%s" % self.port]

        if self.config.get("use_tcp_for_local_connection") or self.config["parameters"].get("unix_socket_directories") == "":
            connection_string.append("host=%s" % self.host)
        elif self.config["parameters"].get("unix_socket_directories"):
            explicit_socket_directory = self.config["parameters"]["unix_socket_directories"].split(" ")[0]
            connection_string.append("host=%s" % explicit_socket_directory)

        return " ".join(connection_string)

    def disconnect(self):
        try:
            self.conn.close()
        except Exception as e:
            logger.error("Error disconnecting: %s" % e)

    def query(self, sql):
        max_attempts = 0
        while True:
            try:
                self.cursor().execute(sql)
                break
            except psycopg2.OperationalError as e:
                if self.conn:
                    self.disconnect()
                self.cursor_holder = None
                if max_attempts > 4:
                    raise e
                max_attempts += 1
                time.sleep(5)
        return self.cursor()

    def data_directory_empty(self):
        return not os.path.exists(self.data_dir) or os.listdir(self.data_dir) == []

    def initialize(self, leader=None):
        if os.system("initdb %s" % self.initdb_options()) == 0 or self.bdr:
            self.write_pg_hba()

            # start Postgres without options to setup replication user indepedent of other system settings
            os.system("pg_ctl start -w -D %s -o '%s'" % (self.data_dir, self.server_options()))
            self.create_replication_user()
            self.create_databases()

            # if BDR is enabled, initialize BDR
            if self.bdr:
                self.initialize_bdr(leader=leader)

            os.system("pg_ctl stop -w -m fast -D %s" % self.data_dir)
            return True

        return False

    def sync_from_leader(self, leader):
        if self.bdr:
            return True

        leader = urlparse(leader["address"])

        f = open("./pgpass", "w")
        f.write("%(hostname)s:%(port)s:*:%(username)s:%(password)s\n" %
                {"hostname": leader.hostname, "port": leader.port, "username": leader.username, "password": leader.password})
        f.close()

        os.system("chmod 600 pgpass")


        return os.system("PGPASSFILE=pgpass pg_basebackup -R -D %(data_dir)s --host=%(host)s --port=%(port)s -U %(username)s" %
                {"data_dir": self.data_dir, "host": leader.hostname, "port": leader.port, "username": leader.username}) == 0

    def initialize_bdr(self, leader=None):
        logging.info('Governor initializing BDR')
        leader = urlparse(leader['address']) if leader is not None else None
        for db in self.databases:
            cur = self.cursor(dbname=db)
            values = {
                'db': db,
                'name': self.name,
                'host': self.host,
                'port': self.port,
                'user': self.replication['username']
            }
            if leader is None:
                cur.execute("""SELECT bdr.bdr_group_create(
                        local_node_name := '%(name)s',
                        node_external_dsn := 'port=%(port)s dbname=%(db)s host=%(host)s user=%(user)s'
                    )""" % values)
                logging.info('Governor: created BDR group.')
                cur.execute('SELECT bdr.bdr_node_join_wait_for_ready()')
                logging.info('Governor: BDR node ready.')
            else:
                values['leader_hostname'] = leader.hostname
                values['leader_port'] = leader.port
                cur.execute("""SELECT bdr.bdr_group_join(
                        local_node_name := '%(name)s',
                        node_external_dsn := 'port=%(port)s dbname=%(db)s host=%(host)s user=%(user)s',
                        join_using_dsn := 'port=%(leader_port)s dbname=%(db)s host=%(leader_hostname)s user=%(user)s'
                    )""" % values)
                logging.info('Governor: joined BDR group.')
                cur.execute('SELECT bdr.bdr_node_join_wait_for_ready()')
                logging.info('Governor: BDR node ready.')
            cur.close()
            self.cursor_holder = None
        logging.info('Governor: BDR initialized.')

    def is_leader(self):
        return not self.query("SELECT pg_is_in_recovery();").fetchone()[0]

    def is_running(self):
        return os.system("pg_ctl status -D %s > /dev/null" % self.data_dir) == 0

    def start(self):
        if self.is_running():
            logger.error("Cannot start PostgreSQL because one is already running.")
            return False

        pid_path = "%s/postmaster.pid" % self.data_dir
        if os.path.exists(pid_path):
            os.remove(pid_path)
            logger.info("Removed %s" % pid_path)

        return os.system("pg_ctl start -w -D %s -o '%s'" % (self.data_dir, self.server_options())) == 0

    def stop(self):
        return os.system("pg_ctl stop -w -D %s -m fast -w" % self.data_dir) != 0

    def reload(self):
        return os.system("pg_ctl reload -w -D %s" % self.data_dir) == 0

    def restart(self):
        return os.system("pg_ctl restart -w -D %s -m fast" % self.data_dir) == 0

    def server_options(self):
        options = "-c listen_addresses=%s -c port=%s" % (self.host, self.port)
        for setting, value in self.config["parameters"].iteritems():
            if value is None:
                continue

            options += " -c \"%s=%s\"" % (setting, value)
        return options

    def initdb_options(self):
        options = "-D %s" % self.data_dir
        if "initdb_parameters" in self.config:
            for param in self.config["initdb_parameters"]:
                options += " \"%s\"" % param
        return options

    def create_databases(self):
        for db in self.databases:
            cur = self.cursor()
            values = (db,)
            # check if db exists
            cur.execute('SELECT 1 from pg_database WHERE datname = %s', values)
            if cur.fetchone() is None:
                # create database
                while True:
                    try:
                        cur.execute('CREATE DATABASE %s' % values)
                        break
                    except psycopg2.OperationalError:
                        time.sleep(5)
                        continue
                logging.info('Governor created database: %s' % values)

            # if bdr is enabled, connect to that DB and set up extensions
            if self.bdr:
                cur.close()
                self.cursor_holder = None
                cur = self.cursor(dbname=db)
                try:
                    cur.execute('CREATE EXTENSION btree_gist; CREATE EXTENSION bdr; CREATE EXTENSION "uuid-ossp"')
                except psycopg2.OperationalError:
                    # these could already exist
                    pass
            cur.close()
            self.cursor_holder = None

    def is_healthy(self):
        if not self.is_running():
            logger.warning("Postgresql is not running.")
            return False
        return True

    def is_healthiest_node(self, state_store):
        last_leader_operation = state_store.last_leader_operation()
        # this should only happen on initialization
        if last_leader_operation is None:
            return True

        xlog_position = self.xlog_position()
        if xlog_position is None or (last_leader_operation - xlog_position) > self.config["maximum_lag_on_failover"]:
            return False

        members = state_store.members()
        if members is None:
            return False

        for member in members:
            if member["hostname"] == self.name:
                continue
            try:
                member_conn = psycopg2.connect(member["address"])
                member_conn.autocommit = True
                member_cursor = member_conn.cursor()
                member_cursor.execute("SELECT %s - (pg_last_xlog_replay_location() - '0/000000'::pg_lsn) AS bytes;" % self.xlog_position())
                xlog_diff = member_cursor.fetchone()[0]
                logger.info([self.name, member["hostname"], xlog_diff])
                if xlog_diff < 0:
                    member_cursor.close()
                    return False
                member_cursor.close()
            except psycopg2.OperationalError:
                continue
        return True

    def clean_slot_name(self, name):
        (name, _) = re.subn(r'[^a-z0-9]+', r'_', name)
        return name

    def replication_slot_name(self):
        return self.clean_slot_name(self.name)

    def write_pg_hba(self):
        f = open("%s/pg_hba.conf" % self.data_dir, "a")
        f.write("host all all all trust\n")
        f.write("host replication %s all trust\n" % self.replication["username"])
        f.close()

    def write_recovery_conf(self, leader_hash):
        if self.bdr:
            return

        f = open("%s/recovery.conf" % self.data_dir, "w")
        f.write("""
standby_mode = 'on'
primary_slot_name = '%(recovery_slot)s'
recovery_target_timeline = 'latest'
""" % {"recovery_slot": self.replication_slot_name()})
        if leader_hash is not None:
            leader = urlparse(leader_hash["address"])
            f.write("""
primary_conninfo = 'user=%(user)s password=%(password)s host=%(hostname)s port=%(port)s sslmode=prefer sslcompression=1'
            """ % {"user": leader.username, "password": leader.password, "hostname": leader.hostname, "port": leader.port})

        if "recovery_conf" in self.config:
            for name, value in self.config["recovery_conf"].iteritems():
                f.write("%s = '%s'\n" % (name, value))
        f.close()

    def follow_the_leader(self, leader_hash):
        if self.bdr:
            return True

        leader = urlparse(leader_hash["address"])
        if os.system("grep 'host=%(hostname)s port=%(port)s' %(data_dir)s/recovery.conf > /dev/null" % {"hostname": leader.hostname, "port": leader.port, "data_dir": self.data_dir}) != 0:
            self.write_recovery_conf(leader_hash)
            self.restart()
        return True

    def follow_no_leader(self):
        if self.bdr:
            return True

        if not os.path.exists("%s/recovery.conf" % self.data_dir) or os.system("grep primary_conninfo %(data_dir)s/recovery.conf &> /dev/null" % {"data_dir": self.data_dir}) == 0:
            self.write_recovery_conf(None)
            if self.is_running():
                self.restart()
        return True

    def promote(self):
        if self.bdr:
            return True
        return os.system("pg_ctl promote -w -D %s" % self.data_dir) == 0

    def demote(self, leader):
        if self.bdr:
            return
        self.write_recovery_conf(leader)
        self.restart()

    def create_replication_user(self):
        self.query("CREATE USER \"%s\" WITH REPLICATION SUPERUSER;" % self.replication["username"])

    def xlog_position(self):
        return self.query("SELECT pg_last_xlog_replay_location() - '0/0000000'::pg_lsn;").fetchone()[0]

    def last_operation(self):
        if self.is_leader():
            return self.query("SELECT pg_current_xlog_location() - '0/00000'::pg_lsn;").fetchone()[0]
        else:
            return self.xlog_position()
