#!/bin/bash
set -e

echo "Creating config file..."

WAL_LEVEL=hot_standby
ARCHIVE_COMMAND='mkdir -p ../wal_archive && cp %p ../wal_archive/%f'
SHARED_PRELOAD_LIBRARIES=''
TRACK_COMMIT_TIMESTAMP="'false'"
DEFAULT_SEQUENCEAM=''
if [ "${BDR_ENABLED}" == 'true' ]; then
  WAL_LEVEL=logical
  SHARED_PRELOAD_LIBRARIES="'bdr'"
  ARCHIVE_COMMAND=''
  TRACK_COMMIT_TIMESTAMP="'true'"
  DEFAULT_SEQUENCEAM="'bdr'"
fi

cat > /governor-config/config.yml << EOF
loop_wait: 10
etcd:
  scope: governor
  ttl: 30
  endpoint: $ETCD_ENDPOINT
  timeout: 5
haproxy_status:
  listen: 0.0.0.0:15432
postgresql:
  name: $HOSTNAME
  listen: ${HOSTNAME}${DOMAIN}:5432
  data_dir: ${PGDATA}/${HOSTNAME}
  databases: ${DATABASES}
  bdr: ${BDR_ENABLED:-false}
  maximum_lag_on_failover: 1048576 # 1 megabyte in bytes
  replication:
    username: ${REPLICATION_USER:-replicator}
    password: ${REPLICATION_PASS:-rep-pass}
    network:  ${REPLICATION_NETWORK:-0.0.0.0/0}
  #recovery_conf:
    #restore_command: cp ../wal_archive/%f %p
  parameters:
    shared_preload_libraries: ${SHARED_PRELOAD_LIBRARIES}
    track_commit_timestamp: ${TRACK_COMMIT_TIMESTAMP}
    default_sequenceam: ${DEFAULT_SEQUENCEAM}
    archive_mode: "on"
    wal_level: ${WAL_LEVEL}
    archive_command: ${ARCHIVE_COMMAND}
    max_wal_senders: 5
    wal_keep_segments: 8
    archive_timeout: 1800s
    max_replication_slots: 5
    hot_standby: "on"
EOF