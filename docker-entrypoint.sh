#!/bin/bash
set -e

mkdir -p "${PGDATA}/${HOSTNAME}"
chown -R postgres:postgres "$PGDATA"
chmod -R 700 "$PGDATA"

governor-setup.sh

./haproxy_status.py /governor-config/config.yml &
exec "$@"